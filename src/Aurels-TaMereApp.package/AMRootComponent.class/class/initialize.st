as yet unclassified
initialize
"self initialize"
	| app |
	super initialize.
	app := WAAdmin register: self asApplicationAt: 'aurels'.
	app exceptionHandler: WAWalkbackErrorHandler.