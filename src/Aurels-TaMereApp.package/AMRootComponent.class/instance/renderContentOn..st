as yet unclassified
renderContentOn: html
	|newProject|
	
	newProject := AMProject new.
	
	html
		text: 'Aurels-TaMereApp';
		break; break;
	
		text: 'Nouveau projet: '.
		
		html form with: [ 
			html textInput on: #projectName of: newProject.
			html submitButton
				callback:  [ 
					AMDatabase DefaultDB addProject: newProject.
					contentComponent currentProject: newProject.
				];
				value: 'Save'.
		].
	
		html break;
	
		render: contentComponent.
      
	"self db projects do: [:project | self renderProject: project asRowOn: html]"