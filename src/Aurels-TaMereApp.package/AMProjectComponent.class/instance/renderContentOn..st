as yet unclassified
renderContentOn: html

	(AMDatabase DefaultDB projects isNotEmpty)
		ifTrue: [
			
			currentProject ifNil: [ currentProject := AMDatabase DefaultDB projects at: 1 ].
			
			
			html div id: #tamere.
			
			html text: 'Projet: '.
			html form: [
  			    	html select 
					list: AMDatabase DefaultDB projects;
  			      	selected: currentProject;
					labels: [:each | each projectName];
  			            callback: [:selectedProject | currentProject := selectedProject ].
				html submitButton
 	 				callback: [ ];
   		      		value: 'OK'
			].

			html heading:  currentProject projectName.
			html anchor 
				callback: [ 
					AMDatabase DefaultDB removeProject: currentProject.
			
					(AMDatabase DefaultDB projects isNotEmpty)
						ifTrue: [ currentProject := AMDatabase DefaultDB projects at: 1. ];
						ifFalse: [ currentProject := AMProject new. ].
				];
				with: '[X]'.
	
	
			html break.
			currentProject logEntries do: [:entry | self renderLogEntry: entry asRowOn: html].
			html break.
			self renderLogEntryFormOn: html.
		].