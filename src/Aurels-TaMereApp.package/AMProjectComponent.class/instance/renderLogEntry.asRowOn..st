as yet unclassified
renderLogEntry: aEntry asRowOn: html
	html text: '-------------------- '.
	html anchor
		callback: [ currentProject removeEntry: aEntry ];
		with: 'Supprimer'.
	html text: ' --------------------'.

	html table:[
		html tableRow: [html tableData: aEntry title].
		html tableRow: [html tableData: aEntry description].
		html tableRow: [html tableData: aEntry date]
	 ].

	html break; break.