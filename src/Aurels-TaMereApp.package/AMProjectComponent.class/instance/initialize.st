as yet unclassified
initialize
	super initialize.
	(AMDatabase DefaultDB projects size > 0)
		ifTrue: [
			currentProject := AMDatabase DefaultDB projects first.
		].
	logEntry := AMLogEntry new.