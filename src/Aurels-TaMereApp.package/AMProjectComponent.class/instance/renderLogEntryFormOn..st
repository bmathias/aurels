as yet unclassified
renderLogEntryFormOn: html
	html heading 
		level: 2;  
		with: 'Add a log entry'.
	html form
		with: [ 
			html textInput on: #title of: self logEntry.
			html textArea on: #description of: self logEntry.
			
			html submitButton
				callback: [ 
					currentProject addEntry: logEntry.
					logEntry := 	AMLogEntry new. ]; 
				with: 'Save Log Entry'.
		].