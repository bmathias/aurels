accessing
addProject: aProject
	|firstEntry|
	
	firstEntry := AMLogEntry new.
	firstEntry title: 'Création du projet'.
	
	aProject addEntry: firstEntry.
	self projects add: aProject.